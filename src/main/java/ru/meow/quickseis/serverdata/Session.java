package ru.meow.quickseis.serverdata;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class Session
{
    public static final int SESSION_LIFETIME = 32000000;
    public static final int REMOVE_SESSION_TIME = -1;
    public static final String REQUEST_COOKIE_NAME = "sessid";
    
    private String id;
    
    
    public Session ()
    {
        id = getRandomString();
    }
    
    // Public methods
    
    public String getId ()
    {
        return id;
    }
    
    
    // Private methods
    
    private static String getRandomString ()
    {
        int length = 32;
        String dictionary = "abcdefghijklmnopqrstuvwxyz012345678901234567890123456789";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            stringBuilder.append(dictionary.charAt((int) (Math.random() * dictionary.length())));
        }
        return stringBuilder.toString();
    }
    
    public static String getSessidFromRequest (HttpServletRequest request)
    {
        Cookie[] cookies = request.getCookies();
        String sessid;
        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if (cookie.getName().equals(Session.REQUEST_COOKIE_NAME))
                {
                    sessid = cookie.getValue();
                    return sessid;
                }
            }
        }
        return null;
    }
}
