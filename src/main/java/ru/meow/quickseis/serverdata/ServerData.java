package ru.meow.quickseis.serverdata;

import ru.meow.quickseis.entity.user.User;

import java.util.HashMap;

public class ServerData
{
    public static final String LOGIN_PAGE_URL = "/login.html";
    public static final String MAIN_PAGE_URL = "/index.html";
    
    public static HashMap<String, User> users = new HashMap<>();
    
}
