package ru.meow.quickseis.exception;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServerException extends Exception
{
    public ServerException () {}
    
    public ServerException (String message)
    {
        super(message);
    }
    
    public static void handleException (Exception e, HttpServletResponse response) throws IOException
    {
        e.printStackTrace();
        response.getWriter().print("Что-то пошло не так");
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
    
    public static void handleException (Exception e)
    {
        e.printStackTrace();
    }
}
