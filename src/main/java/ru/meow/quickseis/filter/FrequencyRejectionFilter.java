package ru.meow.quickseis.filter;


import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;

import com.google.gson.JsonArray;
import ru.meow.quickseis.filter.algorithm.FourierTransform;
import ru.meow.quickseis.util.Complex;

import java.util.ArrayList;
import java.util.List;


public class FrequencyRejectionFilter extends BaseFilter implements Filter
{
    
    //Value of frequency which should be rejected
    private int frequency;
    
    //Width of frequency interval which will be rejected (frequency+/-bandwith/2)
    private int bandwith;
    
    public FrequencyRejectionFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.frequency = arguments.get(0).getAsInt();
        this.bandwith = arguments.get(1).getAsInt();
    }
    
    @Override
    public void apply (TraceList traces)
    {
        traces.sort(sortingTracesFieldName);
        
        for (Trace trace : traces)
        {
            List<Point> points = trace.getPoints();
            List<Complex> complexPoints = new ArrayList<>();
            
            for (Point point : points)
            {
                complexPoints.add(new Complex(point.getX()));
            }
            
            List<Complex> fourierCoefficients = FourierTransform.getFourierCoefficients(complexPoints);
            RejectFrequency(fourierCoefficients, frequency, bandwith);
            complexPoints = FourierTransform.getComplexPoints(fourierCoefficients);
            
            for (int i = 0; i < complexPoints.size(); i++)
            {
                points.get(i).setX(complexPoints.get(i).re);
            }
        }
    }
    
    private void RejectFrequency (List<Complex> fourierCoefficients, int frequency, int bandwith)
    {
        int n = fourierCoefficients.size();
        for (int i = frequency - bandwith / 2; i <= frequency + bandwith / 2; i++)
        {
            if (i > 0 && i < n)
            {
                fourierCoefficients.set(i, new Complex(0));
                fourierCoefficients.set(n - i, new Complex(0));
            }
        }
    }
    
    
    
}
