package ru.meow.quickseis.filter.algorithm;
import ru.meow.quickseis.util.Complex;

import java.util.ArrayList;
import java.util.List;

public class FourierTransform
{
    public static List<Complex> getFourierCoefficients (List<Complex> complexPoints)
    {
        int n = complexPoints.size();
        List<Complex> fourierCoefficients = new ArrayList<>();
        for (int k = 0; k < n; k++)
        {
            Complex totalSum = new Complex(0);
            for (int r = 0; r < n; r++)
            {
                totalSum = totalSum.sum(Complex.exp((new Complex(0, -1)).multiply((float)(2 * Math.PI * k * r / n)))
                        .multiply(complexPoints.get(r)));
            }
            fourierCoefficients.add(totalSum.divide((float)Math.sqrt(n)));
        }
        return fourierCoefficients;
    }
    
    public static List<Complex> getComplexPoints (List<Complex> fourierCoefficients)
    {
        int n = fourierCoefficients.size();
        List<Complex> complexPoints = new ArrayList<>();
        for (int r = 0; r < n; r++)
        {
            Complex totalSum = new Complex(0);
            for (int k = 0; k < n; k++)
            {
                totalSum = totalSum.sum(Complex.exp((new Complex(0, 1)).multiply((float)(2 * Math.PI * k * r / n)))
                        .multiply(fourierCoefficients.get(k)));
            }
            complexPoints.add(totalSum.divide((float)Math.sqrt(n)));
        }
        return complexPoints;
    }
}
