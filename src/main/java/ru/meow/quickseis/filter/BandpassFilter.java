package ru.meow.quickseis.filter;

import com.google.gson.JsonArray;
import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.filter.algorithm.FourierTransform;
import ru.meow.quickseis.util.Complex;

import java.util.ArrayList;
import java.util.List;

public class BandpassFilter extends BaseFilter implements Filter
{
    private int minFrequency;
    private int maxFrequency;
    
    public BandpassFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.minFrequency = arguments.get(0).getAsInt();
        this.maxFrequency = arguments.get(1).getAsInt();
    }
    
    @Override
    public void apply (TraceList traces)
    {
        traces.sort(sortingTracesFieldName);
    
        for (Trace trace : traces)
        {
            List<Point> points = trace.getPoints();
            List<Complex> complexPoints = new ArrayList<>();
        
            for (Point point : points)
            {
                complexPoints.add(new Complex(point.getX()));
            }
        
            List<Complex> fourierCoefficients = FourierTransform.getFourierCoefficients(complexPoints);
            RejectFrequency(fourierCoefficients, minFrequency, maxFrequency);
            complexPoints = FourierTransform.getComplexPoints(fourierCoefficients);
        
            for (int i = 0; i < complexPoints.size(); i++)
            {
                points.get(i).setX(complexPoints.get(i).re);
            }
        }
    }
    
    private void RejectFrequency (List<Complex> fourierCoefficients, int minFrequency, int maxFrequency)
    {
        int n = fourierCoefficients.size();
        for (int i = 1; i < minFrequency; i++)
        {
            fourierCoefficients.set(i, new Complex(0));
            fourierCoefficients.set(n - i, new Complex(0));
        }
        
        for (int i = maxFrequency + 1; i <= n / 2; i++)
        {
            fourierCoefficients.set(i, new Complex(0));
            fourierCoefficients.set(n - i, new Complex(0));
        }
    }
}
