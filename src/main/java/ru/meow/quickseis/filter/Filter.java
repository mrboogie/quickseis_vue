package ru.meow.quickseis.filter;

import ru.meow.quickseis.entity.trace.TraceList;

public interface Filter
{
    String FILTER_NAME_FIELD_NAME = "filter_class_name";
    String SORTING_TRACES_FIELD_NAME = "sorting_traces_field_name";
    String ARGUMENTS_FIELD_NAME = "arguments";
    
    void apply (TraceList traces);
}
