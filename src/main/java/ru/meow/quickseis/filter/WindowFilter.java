package ru.meow.quickseis.filter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.ArrayList;

import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.filter.BaseFilter;
import ru.meow.quickseis.filter.Filter;

public class WindowFilter extends BaseFilter implements Filter
{
    private int windowSize;
    private float maxX;
    
    public WindowFilter (String sortingTracesFieldParameterName, JsonArray arguments)
    {
        super(sortingTracesFieldParameterName);
        this.windowSize = arguments.get(0).getAsInt();
        this.maxX = arguments.get(1).getAsFloat();
    }
    
    public void apply (TraceList traces)
    {
        int currentPartNumber;
        ArrayList<Float> currentMaxXValues = new ArrayList<>();
        for (Trace trace : traces)
        {
            for (Point point : trace.getPoints())
            {
                currentPartNumber = (int) (point.getT() * 1000.0f) / this.windowSize;
                while (currentPartNumber >= currentMaxXValues.size())
                {
                    currentMaxXValues.add(0.0f);
                }
                if (Float.compare(currentMaxXValues.get(currentPartNumber), point.getX()) != -1)
                {
                    continue;
                }
                currentMaxXValues.set(currentPartNumber, point.getX());
            }
        }
        for (Trace trace : traces)
        {
            for (Point point : trace.getPoints())
            {
                currentPartNumber = (int) (point.getT() * 1000.0f) / this.windowSize;
                if (Float.compare(currentMaxXValues.get(currentPartNumber), 0.0f) == 0)
                {
                    continue;
                }
                point.setX(this.maxX / currentMaxXValues.get(currentPartNumber) * point.getX());
            }
        }
    }
}
