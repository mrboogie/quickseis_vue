package ru.meow.quickseis.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import ru.meow.quickseis.database.DatabaseConnector;
import ru.meow.quickseis.entity.fileobject.Seismogram;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.entity.user.User;
import ru.meow.quickseis.filter.Filter;
import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static ru.meow.quickseis.exception.ServerException.handleException;

@WebServlet(urlPatterns = "/filtrate")
public class FiltrateServlet extends GetSeismogramTracesServlet
{
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        try
        {
            TraceList traces = getSeismogramTraces(request);
            
            boolean isPartialFiltration = request.getParameterMap().containsKey(Seismogram.PART_NUMBER_PARAMATER_NAME);
            int seismogramId = Integer.valueOf(request.getParameter(Seismogram.SEISMOGRAM_ID_PARAMATER_NAME));
            
            JsonObject[] filterObjects = new Gson().fromJson(request.getReader(), JsonObject[].class);
    
            DatabaseConnector databaseConnector = new DatabaseConnector();
            int profileId = 0;
            
            if (!isPartialFiltration)
            {
                String sessid = Session.getSessidFromRequest(request);
                User user = ServerData.users.get(sessid);
                databaseConnector.openConnection(user);
                profileId = databaseConnector.getSeismogramProfileId(seismogramId);
            }
    
            DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss");
            
            
            for (JsonObject filterObject : filterObjects)
            {
                String filterClassName = Filter.class.getPackage().toString().substring("package".length() + 1) + "." +
                        filterObject.get(Filter.FILTER_NAME_FIELD_NAME).getAsString();
                
                Constructor constructor = Class.forName(filterClassName).getDeclaredConstructor(String.class, JsonArray.class);
                
                Filter filter = (Filter)(constructor.newInstance(
                        filterObject.get(Filter.SORTING_TRACES_FIELD_NAME).getAsString(),
                        filterObject.getAsJsonArray(Filter.ARGUMENTS_FIELD_NAME))
                );
                
                filter.apply(traces);
                
                if (!isPartialFiltration)
                {
                    Calendar calendar = Calendar.getInstance();
                    
                    databaseConnector.addSeismogram("seismogram" + seismogramId + "_" + dateFormat.format(calendar.getTime()), profileId, traces,
                            Seismogram.SeismogramType.FILTERED);
                    databaseConnector.addSeismogram("seismogram" + seismogramId + "_sum_" + dateFormat.format(calendar.getTime()), profileId,
                            traces.getSum(), Seismogram.SeismogramType.SUM);
                    
                }
            }
    
            if (isPartialFiltration)
            {
                String jsonSeismogramTracesString = traces.getJsonStringView();
                response.getWriter().print(jsonSeismogramTracesString);
            }
            else
            {
                //
            }
        
        }
        catch (Exception e)
        {
            handleException(e, response);
        }
    }
}
