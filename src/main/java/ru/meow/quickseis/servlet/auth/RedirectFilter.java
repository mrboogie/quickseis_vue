package ru.meow.quickseis.servlet.auth;

import ru.meow.quickseis.serverdata.ServerData;
import ru.meow.quickseis.serverdata.Session;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/index.html"})
public class RedirectFilter implements Filter
{
    @Override
    public void init (FilterConfig filterConfig) throws ServletException { }
    
    @Override
    public void doFilter (ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String sessid = Session.getSessidFromRequest(request);
        
        if (sessid != null && ServerData.users.containsKey(sessid))
        {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
            response.sendRedirect(ServerData.LOGIN_PAGE_URL);
        }
    }
    
    @Override
    public void destroy () { }
}
