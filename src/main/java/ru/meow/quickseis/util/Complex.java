package ru.meow.quickseis.util;

public class Complex
{
    public final float re;
    public final float im;
    
    public Complex (float x, float y)
    {
        re = x;
        im = y;
    }
    
    public Complex (float x)
    {
        re = x;
        im = 0;
    }
    
    public Complex multiply (float x)
    {
        return new Complex(this.re * x, this.im * x);
    }
    
    public Complex multiply (Complex z)
    {
        return new Complex(this.re * z.re - this.im * z.im, this.re * z.im + this.im * z.re);
    }
    
    public Complex divide (float x)
    {
        return new Complex(this.re / x, this.im / x);
    }
    
    public Complex sum (Complex z)
    {
        return new Complex(this.re + z.re, this.im + z.im);
    }
    
    public static Complex exp (Complex z)
    {
        return new Complex((float)(Math.exp(z.re) * Math.cos(z.im)), (float)(Math.exp(z.re) * Math.sin(z.im)));
    }
}
