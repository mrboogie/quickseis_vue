package ru.meow.quickseis.entity.trace;

import java.beans.PropertyDescriptor;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

import com.google.common.base.CaseFormat;

import static ru.meow.quickseis.exception.ServerException.handleException;

public class Trace implements Serializable
{
    private int sequenceSeismogramNumber;
    private int fieldSeismogramNumber;
    private int shotPointNumber;
    private int shotPoint;
    private int depthPoint;
    private int offsetPoint;
    private int offsetPointDistance;
    private ArrayList<Point> points;
    
    public int getSequenceSeismogramNumber ()
    {
        return sequenceSeismogramNumber;
    }
    
    public int getFieldSeismogramNumber ()
    {
        return fieldSeismogramNumber;
    }
    
    public int getShotPointNumber ()
    {
        return shotPointNumber;
    }
    
    public int getShotPoint ()
    {
        return shotPoint;
    }
    
    public int getDepthPoint ()
    {
        return depthPoint;
    }
    
    public int getOffsetPoint ()
    {
        return offsetPoint;
    }
    
    public int getOffsetPointDistance ()
    {
        return offsetPointDistance;
    }
    
    public ArrayList<Point> getPoints ()
    {
        return points;
    }
    
    public void setSequenceSeismogramNumber (int sequenceSeismogramNumber)
    {
        this.sequenceSeismogramNumber = sequenceSeismogramNumber;
    }
    
    public void setFieldSeismogramNumber (int fieldSeismogramNumber)
    {
        this.fieldSeismogramNumber = fieldSeismogramNumber;
    }
    
    public void setShotPointNumber (int shotPointNumber)
    {
        this.shotPointNumber = shotPointNumber;
    }
    
    public void setShotPoint (int shotPoint)
    {
        this.shotPoint = shotPoint;
    }
    
    public void setDepthPoint (int depthPoint)
    {
        this.depthPoint = depthPoint;
    }
    
    public void setOffsetPoint (int offsetPoint)
    {
        this.offsetPoint = offsetPoint;
    }
    
    public void setOffsetPointDistance (int offsetPointDistance)
    {
        this.offsetPointDistance = offsetPointDistance;
    }
    
    public void setPoints (ArrayList<Point> points)
    {
        this.points = points;
    }
    
    public Trace (int sequenceSeismogramNumber, int fieldSeismogramNumber, int shotPointNumber, int shotPoint,
                  int depthPoint, int offsetPoint, int offsetPointDistance, ArrayList<Point> points)
    {
        this.sequenceSeismogramNumber = sequenceSeismogramNumber;
        this.fieldSeismogramNumber = fieldSeismogramNumber;
        this.shotPointNumber = shotPointNumber;
        this.shotPoint = shotPoint;
        this.depthPoint = depthPoint;
        this.offsetPoint = offsetPoint;
        this.offsetPointDistance = offsetPointDistance;
        this.points = points;
    }
    
    private ArrayList<Point> deserializePoints (byte[] serializedPoints) throws IOException, ClassNotFoundException
    {
        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(serializedPoints));
        ArrayList<Point> points = (ArrayList<Point>) objectInputStream.readObject();
        objectInputStream.close();
        return points;
    }
    
    public static String getTraceFieldNameFromRequestParameter (String requestParameterName)
    {
        
        String fieldName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, requestParameterName);
        try
        {
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, Trace.class);
            propertyDescriptor.getReadMethod();
            //Trace.class.getField(fieldName);
            return fieldName;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
