package ru.meow.quickseis.entity.trace;

import java.io.Serializable;

public class Point implements Serializable
{
    private float x;
    private float t;
    
    public float getX ()
    {
        return x;
    }
    
    public float getT ()
    {
        return t;
    }
    
    public void setX (float x)
    {
        this.x = x;
    }
    
    public void setT (float t)
    {
        this.t = t;
    }
    
    public Point (float x, float t)
    {
        this.x = x;
        this.t = t;
    }
}
