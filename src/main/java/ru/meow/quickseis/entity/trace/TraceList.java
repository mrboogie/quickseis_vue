package ru.meow.quickseis.entity.trace;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TraceList extends ArrayList<Trace> implements Serializable
{
    public TraceList (@NotNull Collection<? extends Trace> c)
    {
        super(c);
    }
    
    public TraceList () { }
    
    // public String getWebglView ()
    // {
    //     ArrayList<WebglTrace> webglTraces = new ArrayList<>();
    //
    //     for (Trace trace : this)
    //     {
    //         webglTraces.add(new WebglTrace(trace));
    //     }
    //
    //     String jsonTraceListString = new Gson().toJson(webglTraces);
    //     return jsonTraceListString;
    // }
    
    public String getJsonStringView ()
    {
        String jsonTraceListString = new Gson().toJson(this);
        return jsonTraceListString;
    }
    
    public void sort (String sortingFieldName)
    {
        TraceComparator cmp = new TraceComparator(sortingFieldName);
        super.sort(cmp);
    }
    
    private static void skipBytes (LittleEndianDataInputStream dis, int bytesCount) throws IOException
    {
        for (int i = 0; i < bytesCount; i++)
        {
            dis.readByte();
        }
    }
    
    public static TraceList getFromInputStream (InputStream inputStream) throws IOException
    {
        TraceList traces = new TraceList();
        LittleEndianDataInputStream dis = new LittleEndianDataInputStream(inputStream);
        int availableCount = dis.available();
        int readedBytesCount = 0;
        char ch1, ch2;
        int t1, t2, delta;
        int sequenceSeismogramNumber, fieldSeismogramNumber, shotPointNumber, shotPoint, depthPoint, offsetPoint, offsetPointDistance;
        float time;
        ArrayList<Point> points;
    
        while (readedBytesCount < availableCount)
        {
            sequenceSeismogramNumber = dis.readInt();
            skipBytes(dis, 4);
            fieldSeismogramNumber = dis.readInt();
            skipBytes(dis, 4);
            shotPointNumber = dis.readInt();
            shotPoint = dis.readInt();
            depthPoint = dis.readInt();
            offsetPoint = dis.readInt();
            offsetPointDistance = dis.readInt();
            ch1 = (char) dis.readByte();
            ch2 = (char) dis.readByte();
            skipBytes(dis, 10);
            t1 = dis.readInt();
            t2 = dis.readInt();
            delta = dis.readInt();
            skipBytes(dis, 20);
        
            readedBytesCount += 80;
            points = new ArrayList<>();
        
            time = t1 / 1000f;
            for (int i = t1; i < t2; i += delta)
            {
                if (ch1 == 'I' && ch2 == '2')
                {
                    points.add(new Point((float) dis.readShort(), time));
                    readedBytesCount += 2;
                }
                if (ch1 == 'I' && ch2 == '4')
                {
                    points.add(new Point((float) dis.readInt(), time));
                    readedBytesCount += 4;
                }
                if (ch1 == 'R' && ch2 == '2')
                {
                    points.add(new Point((float) dis.readChar(), time));
                    readedBytesCount += 2;
                }
                if (ch1 == 'R' && ch2 == '4')
                {
                    points.add(new Point(dis.readFloat(), time));
                    readedBytesCount += 4;
                }
            
                time += delta / 1000f;
            }
        
            traces.add(new Trace(
                    sequenceSeismogramNumber,
                    fieldSeismogramNumber,
                    shotPointNumber,
                    shotPoint,
                    depthPoint,
                    offsetPoint,
                    offsetPointDistance,
                    points
            ));
        }
        return traces;
    }
    
    public TraceList getSum ()
    {
        TraceList sum = new TraceList();
        this.sort("sequenceSeismogramNumber");
        int pointsCount = this.get(0).getPoints().size();
        
        for (Trace trace: this)
        {
            if (trace.getPoints().size() < pointsCount)
            {
                pointsCount = trace.getPoints().size();
            }
        }
    
        int totalFieldSeismogramNumber = 0;
        int totalShotPointNumber = 0;
        int totalShotPoint = 0;
        int totalDepthPoint = 0;
        int totalOffsetPoint = 0;
        int totalOffsetPointDistance = 0;
        ArrayList<Point> totalPoints = new ArrayList<>();
        for (int i = 0; i < pointsCount; i++)
        {
            totalPoints.add(new Point(0f, 0f));
        }
        
        int oldSequenceSeismogramNumber = -1;
    
        for (int traceNumber = 0; traceNumber <= this.size() ; traceNumber++)
        {
            if (traceNumber == this.size() || this.get(traceNumber).getSequenceSeismogramNumber() != oldSequenceSeismogramNumber)
            {
                if (oldSequenceSeismogramNumber != -1 || traceNumber == this.size())
                {
                    Trace trace = new Trace(
                            oldSequenceSeismogramNumber,
                            totalFieldSeismogramNumber,
                            totalShotPointNumber,
                            totalShotPoint,
                            totalDepthPoint,
                            totalOffsetPoint,
                            totalOffsetPointDistance,
                            totalPoints
                    );
                    
                    sum.add(trace);
                    
                    if (traceNumber == this.size())
                    {
                        break;
                    }
                }
    
                totalFieldSeismogramNumber = 0;
                totalShotPointNumber = 0;
                totalShotPoint = 0;
                totalDepthPoint = 0;
                totalOffsetPoint = 0;
                totalOffsetPointDistance = 0;
                totalPoints = new ArrayList<>();
                
                for (int i = 0; i < pointsCount; i++)
                {
                    totalPoints.add(new Point(0f, 0f));
                }
    
                oldSequenceSeismogramNumber = this.get(traceNumber).getSequenceSeismogramNumber();
            }
    
            totalFieldSeismogramNumber += this.get(traceNumber).getFieldSeismogramNumber();
            totalShotPointNumber += this.get(traceNumber).getShotPointNumber();
            totalShotPoint += this.get(traceNumber).getShotPoint();
            totalDepthPoint += this.get(traceNumber).getDepthPoint();
            totalOffsetPoint += this.get(traceNumber).getOffsetPoint();
            totalOffsetPointDistance += this.get(traceNumber).getOffsetPointDistance();
    
            for (int pointNumber = 0; pointNumber < totalPoints.size(); pointNumber++)
            {
                totalPoints.get(pointNumber).setX(totalPoints.get(pointNumber).getX() + this.get(traceNumber).getPoints().get(pointNumber).getX());
                totalPoints.get(pointNumber).setT(this.get(traceNumber).getPoints().get(pointNumber).getT());
            }
        }
        
        return sum;
    }
    
    
    public TraceList subList (int fromIndex, int toIndex)
    {
        return new TraceList(super.subList(fromIndex, toIndex));
    }
}
