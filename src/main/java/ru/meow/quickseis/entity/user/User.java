package ru.meow.quickseis.entity.user;

import java.sql.*;

import ru.meow.quickseis.database.DatabaseConnector;
import ru.meow.quickseis.exception.ServerException;

import static ru.meow.quickseis.exception.ServerException.handleException;

public class User
{
    public static final String LOGIN_REQUEST_PARAMETER_NAME = "user_login";
    public static final String PASSWORD_REQUEST_PARAMETER_NAME = "user_password";
    
    private String login;
    private String password;
    private int organizationId;
    
    public User (String login, String password)
    {
        this.login = login;
        this.password = password;
        try
        {
            DatabaseConnector databaseConnector = new DatabaseConnector();
            databaseConnector.openConnection(this);
            organizationId = databaseConnector.getUserOrganizationId();
        }
        catch (Exception e)
        {
            handleException(e);
        }
    }
    
    public String getLogin ()
    {
        return login;
    }
    
    public String getPassword ()
    {
        return password;
    }
}
