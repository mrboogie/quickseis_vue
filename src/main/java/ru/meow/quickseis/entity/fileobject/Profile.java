package ru.meow.quickseis.entity.fileobject;

public class Profile
{
    public static final String ID_DB_INNER_LABEL = "id";
    public static final String NAME_DB_LABEL = "name";
    public static final String ID_DB_OUTER_LABEL = "profile_id";
    public static final String FILE_OBJECT_TYPE = "profile";
    public static final String PROFILE_NAME_PARAMATER_NAME = "profile_name";
    public static final String DIRECTORY_ID_PARAMATER_NAME = "directory_id";
    
    private int id;
    private String name;
    private int directoryId;
    
    public Profile (int id, String name, int directoryId)
    {
        this.id = id;
        this.name = name;
        this.directoryId = directoryId;
    }
    
    public int getId ()
    {
        return id;
    }
    
    public String getName ()
    {
        return name;
    }
    
    public int getDirectoryId ()
    {
        return directoryId;
    }
    
    
}
