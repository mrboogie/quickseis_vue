package ru.meow.quickseis.entity.fileobject;

public class Seismogram
{
    public static final String ID_DB_INNER_LABEL = "id";
    public static final String NAME_DB_LABEL = "name";
    public static final String TYPE_DB_LABEL = "type";
    public static final String TRACES_DB_LABEL = "traces";
    public static final String FILE_OBJECT_TYPE = "seismogram";
    public static final String SEISMOGRAM_NAME_PARAMATER_NAME = "seismogram_name";
    public static final String PROFILE_ID_PARAMATER_NAME = "profile_id";
    public static final String SEISMIC_FILE_PARAMATER_NAME = "seismic_file";
    public static final String PART_NUMBER_PARAMATER_NAME = "part_number";
    public static final String SEISMOGRAM_ID_PARAMATER_NAME = "seismogram_id";
    public static final String TRACE_MAX_COUNT_PARAMATER_NAME = "trace_max_count";
    public static final String SORTING_FIELD_NAME_PARAMATER_NAME = "sorting_field_name";
    
    private int id;
    private String name;
    private int profileId;
    private SeismogramType seismogramType;
    
    public enum SeismogramType
    {
        BASE, FILTERED, SUM;
        
        @Override
        public String toString ()
        {
            switch (this)
            {
                case BASE: return "base";
                case FILTERED: return "filtered";
                case SUM: return "sum";
            }
            
            return null;
        }
        
        public static SeismogramType fromString (String s)
        {
            for (SeismogramType seismogramType : SeismogramType.values())
            {
                if (seismogramType.toString().equals(s))
                {
                    return seismogramType;
                }
            }
    
            return null;
        }
    }
    
    public Seismogram (int id, String name, int profileId, SeismogramType seismogramType)
    {
        this.id = id;
        this.name = name;
        this.profileId = profileId;
        this.seismogramType = seismogramType;
    }
    
    public int getId ()
    {
        return id;
    }
    
    public String getName ()
    {
        return name;
    }
    
    public int getProfileId ()
    {
        return profileId;
    }
    
    public SeismogramType getSeismogramType ()
    {
        return seismogramType;
    }
}
