package ru.meow.quickseis.database;

import com.google.common.base.CaseFormat;
import com.google.common.io.LittleEndianDataInputStream;
import com.mysql.fabric.jdbc.FabricMySQLDriver;
import javafx.beans.property.Property;
import ru.meow.quickseis.entity.fileobject.Directory;
import ru.meow.quickseis.entity.organization.Organization;
import ru.meow.quickseis.entity.fileobject.Profile;
import ru.meow.quickseis.entity.fileobject.Seismogram;
import ru.meow.quickseis.entity.trace.Trace;
import ru.meow.quickseis.entity.trace.TraceList;
import ru.meow.quickseis.entity.user.User;
import ru.meow.quickseis.exception.ServerException;
import ru.meow.quickseis.entity.trace.Point;
import ru.meow.quickseis.jstree.JsTreeNode;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;

import static ru.meow.quickseis.exception.ServerException.handleException;

public class DatabaseConnector
{
    private static final String DATABASE_URL = "jdbc:mysql://195.69.157.72:3306/seismograms?useUnicode=true&characterEncoding=utf8";
    
    private Connection connection;
    
    public DatabaseConnector () {}
    
    public void openConnection () throws NamingException, SQLException
    {
        InitialContext initialContext = new InitialContext();
        DataSource dataSource = (DataSource) initialContext.lookup("java:/comp/env/jdbc/test");
        connection = dataSource.getConnection();
    }
    
    public void openConnection (User user) throws ServerException
    {
        try
        {
            Driver driver = new FabricMySQLDriver();
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DATABASE_URL, user.getLogin(), user.getPassword());
        }
        catch (Exception e)
        {
            handleException(e);
            throw new ServerException();
        }
    }
    
    public void closeConnection () throws SQLException
    {
        connection.close();
    }
    
    public boolean isUserExist (String userLogin, String userPassword) throws SQLException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = String.format("CALL get_users_count('%s', '%s')", userLogin, userPassword);
        ResultSet resultSet = statement.executeQuery(query);
        int usersCount;
        if (resultSet.next())
        {
            usersCount = resultSet.getInt("users_count");
            if (usersCount == 1)
            {
                return true;
            }
            else if (usersCount == 0)
            {
                return false;
            }
            else
            {
                throw new ServerException();
            }
        }
        else
        {
            throw new ServerException();
        }
    }
    
    public int getLastInsertId () throws SQLException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = "SELECT LAST_INSERT_ID() AS id";
        ResultSet resultSet = statement.executeQuery(query);
        if (resultSet.next())
        {
            return resultSet.getInt("id");
        }
        else
        {
            throw new ServerException();
        }
    }
    
    public int getUserOrganizationId () throws SQLException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = "SELECT get_user_organization_id()";
        ResultSet resultSet = statement.executeQuery(query);
        int organizationId;
        if (resultSet.next())
        {
            organizationId = resultSet.getInt(Organization.ID_DB_OUTER_LABEL);
            return organizationId;
        }
        else
        {
            throw new ServerException();
        }
    }
    
    
    // Directory
    
    
    public int addDirectory (String directoryName, Integer parentDirectoryId) throws SQLException, ServerException
    {
        Statement statement = connection.createStatement();
        String query;
        if (parentDirectoryId == null)
        {
            query = String.format("CALL add_directory('%s', NULL)", directoryName);
        }
        else
        {
            query = String.format("CALL add_directory('%s', '%d')", directoryName, parentDirectoryId);
        }
        statement.executeUpdate(query);
        return getLastInsertId();
    }
    
    public ArrayList<JsTreeNode> getAllUserFileObjectsAsJsTreeNodes () throws SQLException
    {
        ArrayList<JsTreeNode> jsTreeNodes = new ArrayList<>();
        Statement getAllDirectoriesStatement = connection.createStatement();
        String getAllDirectoriesQuery = "call get_all_user_directories";
        ResultSet directoryResultSet = getAllDirectoriesStatement.executeQuery(getAllDirectoriesQuery);
    
        while (directoryResultSet.next())
        {
            int directoryId = directoryResultSet.getInt(Directory.ID_DB_INNER_LABEL);
            Integer parentDirectoryId = (Integer)directoryResultSet.getObject(Directory.PARENT_ID_DB_LABEL);
            String directoryName = directoryResultSet.getString(Directory.NAME_DB_LABEL);
            jsTreeNodes.add(new JsTreeNode(
                    Directory.FILE_OBJECT_TYPE + directoryId,
                    parentDirectoryId == null ? "#" : Directory.FILE_OBJECT_TYPE + parentDirectoryId,
                    directoryName,
                    Directory.FILE_OBJECT_TYPE
            ));
        }
    
        Statement getAllProfilesStatement = connection.createStatement();
        String getAllProfilesQuery = "call get_all_user_profiles";
        ResultSet profileResultSet = getAllProfilesStatement.executeQuery(getAllProfilesQuery);
        
        while (profileResultSet.next())
        {
            int profileId = profileResultSet.getInt(Profile.ID_DB_INNER_LABEL);
            int parentDirectoryId = profileResultSet.getInt(Directory.ID_DB_OUTER_LABEL);
            String profileName = profileResultSet.getString(Profile.NAME_DB_LABEL);
            jsTreeNodes.add(new JsTreeNode(
                    Profile.FILE_OBJECT_TYPE + profileId,
                    Directory.FILE_OBJECT_TYPE + parentDirectoryId,
                    profileName,
                    Profile.FILE_OBJECT_TYPE
            ));
        }
    
        Statement getAllSeismogramsStatement = connection.createStatement();
        String getAllSeismogramsQuery = "call get_all_user_seismograms";
        ResultSet seismogramResultSet = getAllSeismogramsStatement.executeQuery(getAllSeismogramsQuery);
        
        while (seismogramResultSet.next())
        {
            int seismogramId = seismogramResultSet.getInt(Seismogram.ID_DB_INNER_LABEL);
            int parentProfileId = seismogramResultSet.getInt(Profile.ID_DB_OUTER_LABEL);
            String seismogramJsTreeType = seismogramResultSet.getString(Seismogram.TYPE_DB_LABEL) +
                    CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, Seismogram.FILE_OBJECT_TYPE);
            String seismogramName = seismogramResultSet.getString(Seismogram.NAME_DB_LABEL);
            
            jsTreeNodes.add(new JsTreeNode(
                    seismogramJsTreeType + seismogramId,
                    Profile.FILE_OBJECT_TYPE + parentProfileId,
                    seismogramName,
                    seismogramJsTreeType
            ));
        }
        
        return jsTreeNodes;
    }
    
    public int addProfile (String profileName, int directoryId) throws SQLException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = String.format("CALL add_profile('%s', '%d')", profileName, directoryId);
        statement.executeUpdate(query);
        return getLastInsertId();
    }
    
    public TraceList getSeismogramTraces (int seismogramId, String sortingFieldName)
            throws SQLException, IOException, ClassNotFoundException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = String.format("CALL get_seismogram('%d')", seismogramId);
        ResultSet resultSet = statement.executeQuery(query);
        
        if (resultSet.next())
        {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(
                    resultSet.getBytes(Seismogram.TRACES_DB_LABEL)));
    
            TraceList traces = (TraceList) objectInputStream.readObject();
            objectInputStream.close();
            statement.close();
    
            if (sortingFieldName != null)
            {
                traces.sort(sortingFieldName);
            }
            
            return traces;
        }
        else
        {
            throw new ServerException();
        }
    }
    
    public TraceList getSeismogramTraces (int seismogramId, String sortingFieldName, int partNumber)
            throws SQLException, IOException, ClassNotFoundException, ServerException, IntrospectionException,
            InvocationTargetException, IllegalAccessException
    {
        Statement statement = connection.createStatement();
        String query = String.format("CALL get_seismogram('%d')", seismogramId);
        ResultSet resultSet = statement.executeQuery(query);
        TraceList traces;
        
        if (resultSet.next())
        {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(
                    resultSet.getBytes(Seismogram.TRACES_DB_LABEL)));
        
            traces = (TraceList) objectInputStream.readObject();
            objectInputStream.close();
            statement.close();
            if (sortingFieldName == null)
            {
                sortingFieldName = "sequenceSeismogramNumber";
            }
        }
        else
        {
            throw new ServerException();
        }
        
        traces.sort(sortingFieldName);
    
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(sortingFieldName, Trace.class);
        
        
        int fieldValue = -1, fieldOldValue, currentPartNumber = -1;
        TraceList tracePart = new TraceList();
        
        for (Trace trace : traces)
        {
            fieldOldValue = fieldValue;
            fieldValue = (Integer)propertyDescriptor.getReadMethod().invoke(trace);
            if (fieldValue != fieldOldValue)
            {
                currentPartNumber++;
            }
            if (currentPartNumber > partNumber)
            {
                break;
            }
            if (currentPartNumber < partNumber)
            {
                continue;
            }
            
            tracePart.add(trace);
        }
        
        return tracePart;
    }
    
    
    public TraceList getSeismogramTraces (int seismogramId, String sortingFieldName, int partNumber, int traceMaxCount)
            throws SQLException, IOException, ClassNotFoundException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = String.format("CALL get_seismogram('%d')", seismogramId);
        ResultSet resultSet = statement.executeQuery(query);
        if (resultSet.next())
        {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(resultSet.getBytes("traces")));
            TraceList traces = (TraceList) objectInputStream.readObject();
            objectInputStream.close();
            statement.close();
            if (sortingFieldName == null)
            {
                sortingFieldName = "sequenceSeismogramNumber";
            }
        
            traces.sort(sortingFieldName);
            if (Math.ceil((double) ((float) traces.size() / (float) traceMaxCount)) <= (double) partNumber)
            {
                return null;
            }
            else
            {
                TraceList tracePart = traces.subList(traceMaxCount * partNumber, Math.min(traceMaxCount * (partNumber + 1), traces.size()));
                return tracePart;
            }
        }
        else
        {
            throw new ServerException();
        }
    }
    
    public int addSeismogram (String seismogramName, int profileId, TraceList traces, Seismogram.SeismogramType seismogramType)
            throws SQLException, ServerException, IOException
    {
        String addSeismogramQuery = "CALL add_seismogram(?, ?, ?, ?)";
        PreparedStatement addSeismogramStatement = connection.prepareStatement(addSeismogramQuery);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(baos);
        out.writeObject(traces);
        
        addSeismogramStatement.setString(1, seismogramName);
        addSeismogramStatement.setInt(2, profileId);
        addSeismogramStatement.setString(3, seismogramType.toString());
        addSeismogramStatement.setBytes(4, baos.toByteArray());
        addSeismogramStatement.executeUpdate();
        out.close();
        baos.close();
        addSeismogramStatement.close();
        
        int seismogramId = getLastInsertId();
        return seismogramId;
    }
    
    public int getSeismogramProfileId (int seismogramId) throws SQLException, ServerException
    {
        Statement statement = connection.createStatement();
        String query = String.format("call get_seismogram_profile_id ('%d')", seismogramId);
        ResultSet resultSet = statement.executeQuery(query);
        if (resultSet.next())
        {
            return resultSet.getInt(Seismogram.PROFILE_ID_PARAMATER_NAME);
        }
        else
        {
            throw new ServerException();
        }
    }
}
