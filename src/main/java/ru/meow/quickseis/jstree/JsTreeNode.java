package ru.meow.quickseis.jstree;

public class JsTreeNode
{
    private String id;
    private String parent;
    private String text;
    private String type;

    public JsTreeNode (String id, String parent, String text, String type)
    {
        this.id = id;
        this.parent = parent;
        this.text = text;
        this.type = type;
    }
}
