const requireComponent = require.context('../components/filters', false, /\w+\.(vue|js)$/);
export let filterComponentsObject = {};
export let filterComponentsArray = [];

requireComponent.keys().forEach(fileName =>
{
	const componentConfig = requireComponent(fileName);
	const componentName = fileName.replace(/^\.\/(.*)\.\w+$/, '$1');
	filterComponentsObject[componentName] = componentConfig.default || componentConfig;
	filterComponentsArray.push(componentConfig.default || componentConfig)
});

