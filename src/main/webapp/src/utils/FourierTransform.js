import Complex from './Complex'

export default class FourierTransform
{
    static getFourierCoefficients (complexPoints)
    {
        var n = complexPoints.length;
        var fourierCoefficients = [];
        for (var k = 0; k < n; k++)
        {
            var totalSum = new Complex(0);
            for (var r = 0; r < n; r++)
            {
                totalSum = totalSum.sum(Complex.exp((new Complex(0, -1)).multiplyX(2 * Math.PI * k * r / n))
                        .multiplyZ(complexPoints[r]));
            }
            fourierCoefficients.push(totalSum.divide(Math.sqrt(n)));
        }
        return fourierCoefficients;
    }
    
    static getComplexPoints (fourierCoefficients)
    {
        var n = fourierCoefficients.length;
        var complexPoints = [];
        for (var r = 0; r < n; r++)
        {
            var totalSum = new Complex(0);
            for (var k = 0; k < n; k++)
            {
                totalSum = totalSum.sum(Complex.exp((new Complex(0, -1)).multiplyX(2 * Math.PI * k * r / n))
                        .multiplyZ(fourierCoefficients[k]));
            }
            complexPoints.push(totalSum.divide(Math.sqrt(n)));
        }
        return complexPoints;
    }
}