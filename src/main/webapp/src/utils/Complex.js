export default class Complex
{
    constructor (re, im)
    {
        this.re = re;
        this.im = im == undefined ? 0 : im;
    }
    
    getDistance ()
    {
        return Math.sqrt(Math.pow(this.re, 2) + Math.pow(this.im, 2));
    }
    
    multiplyX (x)
    {
        return new Complex(this.re * x, this.im * x);
    }
    
    multiplyZ (z)
    {
        return new Complex(this.re * z.re - this.im * z.im, this.re * z.im + this.im * z.re);
    }
    
    divide (x)
    {
        return new Complex(this.re / x, this.im / x);
    }
    
    sum (z)
    {
        return new Complex(this.re + z.re, this.im + z.im);
    }
    
    static exp (z)
    {
        return new Complex(Math.exp(z.re) * Math.cos(z.im), Math.exp(z.re) * Math.sin(z.im));
    }
}