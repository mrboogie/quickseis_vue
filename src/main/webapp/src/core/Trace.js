import TraceField from './TraceField'
import WebglTrace from './WebglTrace'

export default class Trace
{
    constructor (traceObject)
    {
        this.sequenceSeismogramNumber = traceObject[TraceField.SEQUENCE_SEISMOGRAM_NUMBER];
        this.fieldSeismogramNumber = traceObject[TraceField.FIELD_SEISMOGRAM_NUMBER];
        this.shotPointNumber = traceObject[TraceField.SHOT_POINT_NUMBER];
        this.shotPoint = traceObject[TraceField.SHOT_POINT];
        this.depthPoint = traceObject[TraceField.DEPTH_POINT];
        this.offsetPoint = traceObject[TraceField.OFFSET_POINT];
        this.offsetPointDistance = traceObject[TraceField.OFFSET_POINT_DISTANCE];
        this.points = traceObject[TraceField.POINTS].map(a => Object.assign({}, a));
    }
    
    clone ()
    {
        let trace = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
        trace.points = this.points.map(a => Object.assign({}, a));
        return trace;
    }
    
    getWebglView (tMin, tMax)
    {
    	tMin /= 1000;
    	tMax /= 1000;
    	
        let trace = this.clone();
        let points = trace[TraceField.POINTS];
        
        let i = 0;
        while (i < points.length)
        {
            if (points[i].t < tMin || points[i].t > tMax)
            {
                points.splice(i, 1);
            }
            else
            {
                i++;
            }
        }
        
        return new WebglTrace(trace);
    }
}