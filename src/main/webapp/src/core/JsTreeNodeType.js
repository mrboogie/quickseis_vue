export  default class JsTreeNodeType {}
JsTreeNodeType.DIRECTORY = "directory";
JsTreeNodeType.PROFILE = "profile";
JsTreeNodeType.BASE_SEISMOGRAM = "baseSeismogram";
JsTreeNodeType.FILTERED_SEISMOGRAM = "filteredSeismogram";
JsTreeNodeType.SUM_SEISMOGRAM = "sumSeismogram";