export default class TraceField {}
TraceField.SEQUENCE_SEISMOGRAM_NUMBER = "sequenceSeismogramNumber";
TraceField.FIELD_SEISMOGRAM_NUMBER = "fieldSeismogramNumber";
TraceField.SHOT_POINT_NUMBER = "shotPointNumber";
TraceField.SHOT_POINT = "shotPoint";
TraceField.DEPTH_POINT = "depthPoint";
TraceField.OFFSET_POINT = "offsetPoint";
TraceField.OFFSET_POINT_DISTANCE = "offsetPointDistance";
TraceField.POINTS = "points";

// const TraceField = {
// 	SEQUENCE_SEISMOGRAM_NUMBER: 'sequenceSeismogramNumber',
// 	FIELD_SEISMOGRAM_NUMBER: 'fieldSeismogramNumber',
// 	SHOT_POINT_NUMBER: 'shotPointNumber',
// 	SHOT_POINT: 'shotPoint',
// 	DEPTH_POINT: 'depthPoint',
// 	OFFSET_POINT: 'offsetPoint',
// 	OFFSET_POINT_DISTANCE: 'offsetPointDistance',
// 	POINTS: 'points'
// };
//
// export default TraceField;