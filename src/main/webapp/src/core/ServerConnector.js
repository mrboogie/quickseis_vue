export default class ServerConnector
{
    static addSeismogram (form, jstreeDirectoryId)
    {
        var profileName = form.elements["profile_name"].value;
        var directoryId = jstreeDirectoryId.substring(JsTree.NODE_TYPE_DIRECTORY.length);

        var query = "/add_file_object?file_object_type=" + JsTree.NODE_TYPE_PROFILE +
            "&profile_name=" + encodeURIComponent(profileName) +
            "&directory_id=" + directoryId;

        var profileId;

        $.post(query)
            .then(function (result)
            {
                profileId = result;
                var jstreeProfileId = JsTree.NODE_TYPE_PROFILE + profileId;

                ApplicationData.jstree.addNode(
                    jstreeProfileId,
                    jstreeDirectoryId,
                    profileName,
                    JsTree.NODE_TYPE_PROFILE
                );

                var formData = new FormData(form);
                formData.append("profile_id", profileId);

                return $.post({
                    url: "/add_file_object?file_object_type=seismogram",
                    data: formData,
                    processData: false,
                    contentType: false
                })
            })
            .then(function (seismogramId)
            {
                var seismogramName = form.elements["seismogram_name"].value;
                var jstreeSeismogramId = JsTree.NODE_TYPE_BASE_SEISMOGRAM + seismogramId;
                var jstreeProfileId = JsTree.NODE_TYPE_PROFILE + profileId;

                ApplicationData.jstree.addNode(
                    jstreeSeismogramId,
                    jstreeProfileId,
                    seismogramName,
                    JsTree.NODE_TYPE_BASE_SEISMOGRAM
                );
            })
            .catch(function (reason)
            {
                console.log(reason);
            });
    }

    static getTracesFromServer (seismogramId, sortingTraceField, partNumber)
    {
        var query = "/get_seismogram_traces?seismogram_id=" + seismogramId +
                "&sorting_field_name=" + sortingTraceField +
                (partNumber == null ? "" : "&part_number=" + partNumber) +
                (partNumber != null && Seismogram.traceMaxCount != 0 ? "&trace_max_count=" + Seismogram.traceMaxCount : "");

        return $.post(query);
    }

    static addDirectory (form, jstreeParentDirectoryId)
    {
        var directoryName = form.elements["directory_name"].value;
        var parentDirectoryId;

        if (jstreeParentDirectoryId != undefined)
        {
            parentDirectoryId = jstreeParentDirectoryId.substring(JsTree.NODE_TYPE_DIRECTORY.length);
        }

        var query = "/add_file_object?file_object_type=" + JsTree.NODE_TYPE_DIRECTORY +
            "&directory_name=" + encodeURIComponent(directoryName) +
            (parentDirectoryId != undefined ? "&parent_directory_id=" + parentDirectoryId : "");

        $.post({
            url: query,
            success: function (directoryId)
            {
                var jstreeDirectoryId = JsTree.NODE_TYPE_DIRECTORY + directoryId;
                jstreeParentDirectoryId = jstreeParentDirectoryId == undefined ? "#" : jstreeParentDirectoryId;

                ApplicationData.jstree.addNode(
                    jstreeDirectoryId,
                    jstreeParentDirectoryId,
                    directoryName,
                    JsTree.NODE_TYPE_DIRECTORY
                );
            },
            error: function (xhr)
            {
                console.log(xhr.responseText);
            }
        });
    }
}