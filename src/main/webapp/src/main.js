import Vue from 'vue'
import App from './App.vue'

Vue.prototype.$screenRatio = window.devicePixelRatio;

new Vue({
	el: '#app',
	render: h => h(App)
});
