import TraceField from '../core/TraceField'

export default {
	data ()
	{
		return {
			sortingTraceFields: [
				{
					value: TraceField.SHOT_POINT,
					text: 'SP'
				},
				{
					value: TraceField.DEPTH_POINT,
					text: 'DP'
				},
				{
					value: TraceField.OFFSET_POINT,
					text: 'OP'
				},
			],
			
			isOpened: false,
			
			isEnabled: true,
			
			isSelected: false
		}
	}
}